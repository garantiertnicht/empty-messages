name := "bot"

version := "0.1"

scalaVersion := "2.12.8"
libraryDependencies += "net.katsstuff" %% "ackcord-data" % "0.14.0"
libraryDependencies += "net.katsstuff" %% "ackcord-core" % "0.14.0"
