# Empty Quotes

Empty Quotes is a discord bot which detects various uses of markdown which
present empty messages.

## Running
To run this bot:
* (Optional) Change the invite URL. Yes, you need to change the code for that.
  Please change the invite if you host a modified version as we do not know
  how to give advice for your modifications
* Provide your token as a command line argument when starting. No, I haven't
  provided one for you (:

## Links
* [Add the official instance](https://discordapp.com/oauth2/authorize?client_id=603621003284709407&permissions=9216&redirect_uri=https%3A%2F%2Fdiscord.gg%2FSfqF6k9&scope=bot&response_type=code)
* [Guild of the Developer](https://discord.gg/SfqF6k9)