/*
 * Empty Quotes is a discord bot to automagically delete some empty messages
 * Copyright (C) 2019 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import ackcord.data.Permission
import ackcord.data.raw.RawActivity
import ackcord.gateway.GatewaySettings
import ackcord.requests.{BotAuthentication, RequestHelper}
import ackcord.syntax._
import ackcord.{APIMessage, Cache, DiscordShard}
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Materializer, Supervision}

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext

object src extends App {
  val token = args.head // Imagine it being here

  val decider: Supervision.Decider = {
    case _: Exception => Supervision.Resume
    case _ => Supervision.Stop
  }

  implicit val system: ActorSystem = ActorSystem("AckCord")
  implicit val mat: Materializer = ActorMaterializer(ActorMaterializerSettings(system)
    .withDebugLogging(true)
    .withSupervisionStrategy(decider)
  )
  implicit val executionerContext: ExecutionContext = system.dispatcher

  val cache = Cache.create
  val requests = RequestHelper.create(BotAuthentication(token))

  /**
    * Checks if a message is bad
    * @param content The message to check
    * @return
    */
  @tailrec
  private def isBad(content: String): Boolean = {
    /**
      * Check if the content matches
      * @param contentRemaining The content to check
      * @param expression The expression to validate against
      * @param cont If we shall continue
      * @return None if it does not match, Some if it does containing the
      *         remaining string
      */
    @tailrec
    def check(contentRemaining: String, expression: String, cont: Boolean = false): Option[String] = {
      if (
        expression.isEmpty
      ) {
        return Some(contentRemaining)
      }

      val char = contentRemaining.head
      expression.head match {
        case ' ' =>
          if (char.isWhitespace) {
            check(contentRemaining.tail, expression, true)
          } else if (cont) {
            check(contentRemaining, expression.tail)
          } else {
            None
          }
        case expected =>
          if (expected == char) {
            check(contentRemaining.tail, expression.tail)
          } else {
            None
          }
      }
    }

    // If this needs to be super-efficient, build a tree out of these.
    // For now, it does not need to be super efficient.
    val badExpressions = Stream(
      "** **",
      "*** ***",
      "_ _",
      "*_ _*",
      "**_ _**",
      "***_ _***",
      "_* *_",
      "_** **_",
      "_*** ***_"
    )

    @tailrec
    def trim(content: String): String =
      if (!content.isEmpty && content.head.isWhitespace) trim(content.tail)
      else content

    badExpressions.map(bad => check(content, bad)).find(_.isDefined) match {
      case None => false
      case Some(Some(remaining)) =>
        val trimmed = trim(remaining)
        if (trimmed.isEmpty) {
          true
        } else {
          isBad(trimmed)
        }
      case _ =>
        throw new AssertionError("This should never happen.")
    }
  }

  cache.subscribeAPI.filter({
    case event: APIMessage.MessageCreate =>
      implicit val snapshot = event.cache.current

      if (
        event.message.embeds.nonEmpty ||
        event.message.attachment.nonEmpty ||
        event.message.guild.isEmpty
      ) {
        false
      } else {
        val selfMember = event.message.guild.getOrElse(null).members(snapshot.botUser.id)
        selfMember.channelPermissionsId(
              event.message.guild.getOrElse(null),
              event.message.channelId
            ).hasPermissions(Permission.ManageMessages) &&
        isBad(event.message.content)
      }
    case _ =>
        false
  }).map({
    case event: APIMessage.MessageCreate =>
      event.message.delete()
    case _ =>
      throw new AssertionError("This should never happen.")
  }).runWith(requests.sinkIgnore)

  cache.subscribeAPI.collect({
    case event: APIMessage.MessageCreate =>
      if (event.message.isAuthorUser) {
        val content = event.message.content
        val id = event.cache.current.botUser.id
        implicit val cache = event.cache.current

        if (content.startsWith(s"<@$id>") || content.startsWith(s"<@!$id>")) {
          Option(
            event.message.tGuildChannel.map(
              _.sendMessage(
                """Hello, I'm a friendly neighborhood bot which cleans up empty
                  |messages such as `** **` or `_ _*** ***`. For help and
                  |support, you can join our support server:
                  |<https://discord.gg\SfqF6k9>. You can find the most
                  |up-to-date invite in the pinned messages there.
                """.stripMargin.lines.mkString(" ")
              )
            ).getOrElse(null)
          )
        } else {
          None
        }
      } else None
  }).collect({
    case Some(x) => x
  }).runWith(requests.sinkIgnore)

  val gatewaySettings = GatewaySettings(
    token,
    activity = Some(RawActivity(
      "discord.gg/SfqF6k9",
      3,
      None,
      None,
      None,
      None,
      None,
      None,
      None
    )))
  DiscordShard.fetchWsGatewayWithShards(token).foreach { case (url, shards) =>
    val shard = DiscordShard.connectMultiple(url, shards, gatewaySettings, cache, "Shard")
    DiscordShard.startShards(shard)
  }

}
